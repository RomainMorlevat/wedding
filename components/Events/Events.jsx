import { useState } from 'react'
import { ClockIcon, LocationMarkerIcon } from '@heroicons/react/outline'

const CeremonyTab = () => {
  return (
    <ul>
      <li>
        <ClockIcon className='inline-block text-flowerGreen h-5 w-5' /> 11:00 - 12:00
      </li>
      <li>
        <LocationMarkerIcon className='inline-block text-flowerGreen h-5 w-5' /> City hall
      </li>
    </ul>
  )
}

const DinnerTab = () => {
  return (
    <ul>
      <li>
        <ClockIcon className='inline-block text-flowerGreen h-5 w-5' /> 17:00 - 21:00
      </li>
      <li>
        <LocationMarkerIcon className='inline-block text-flowerGreen h-5 w-5' /> W Hotel
      </li>
    </ul>
  )
}

const PartyTab = () => {
  return (
    <ul>
      <li>
        <ClockIcon className='inline-block text-flowerGreen h-5 w-5' /> 21:00 - 00:00
      </li>
      <li>
        <LocationMarkerIcon className='inline-block text-flowerGreen h-5 w-5' /> W Hotel
      </li>
    </ul>
  )
}

const tabs = [<CeremonyTab key={0} />, <DinnerTab key={1} />, <PartyTab key={2} />]

const Events = () => {
  const [activeTabIndex, setActiveTabIndex] = useState(0)

  return (
    <div className='m-auto text-justify w-10/12 md:w-6/12'>
      <h2 className='text-2xl text-slate-700 text-center font-bold'>Wedding events</h2>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua.
      </p>

      <div className='inline-flex justify-around py-2 px-1 w-full text-xl text-slate-700'>
        <button
          className={
            activeTabIndex === 0
              ? 'border-b border-flowerGreen font-bold'
              : 'text-flowerGray hover:text-black'
          }
          onClick={() => setActiveTabIndex(0)}
        >
          Ceremony
        </button>
        <button
          className={
            activeTabIndex === 1
              ? 'border-b border-flowerGreen font-bold'
              : 'text-flowerGray hover:text-black'
          }
          onClick={() => setActiveTabIndex(1)}
        >
          Dinner
        </button>
        <button
          className={
            activeTabIndex === 2
              ? 'border-b border-flowerGreen font-bold'
              : 'text-flowerGray hover:text-black'
          }
          onClick={() => setActiveTabIndex(2)}
        >
          Party
        </button>
      </div>

      <div className='text-l px-5'>{tabs[activeTabIndex]}</div>
    </div>
  )
}

export default Events
