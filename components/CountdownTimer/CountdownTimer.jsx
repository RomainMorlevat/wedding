import { useEffect, useState } from 'react'

const formatDate = (countDown) => {
  const days = Math.floor(countDown / (1000 * 60 * 60 * 24))
  const hours = Math.floor(
    (countDown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  )
  const minutes = Math.floor((countDown % (1000 * 60 * 60)) / (1000 * 60))
  const seconds = Math.floor((countDown % (1000 * 60)) / 1000)

  return [days, hours, minutes, seconds]
}

const CountdownTimer = () => {
  const finalDate = new Date(process.env.NEXT_PUBLIC_COUNTDOWN_DATE).getTime()

  const [currentCountdownDate, setCurrentCountdownDate] = useState(formatDate(finalDate - new Date().getTime()))

  useEffect(() => {
    const interval = setInterval(() => {
      const currentCountdownDateInSeconds = finalDate - new Date().getTime()
      const currentCountdownDateFormatted = formatDate(currentCountdownDateInSeconds)
      setCurrentCountdownDate(currentCountdownDateFormatted)
    }, 1000)

    return () => clearInterval(interval)
  }, [finalDate])

  if (currentCountdownDate.reduce((partialSum, acc) => partialSum + acc, 0) <= 0) {
    return (
      <div className='text-center'>
        The wedding has already taken place.
      </div>
    )
  } else {
    return (
      <div className='flex flex-row justify-center'>
        <div className='flex flex-col items-center p-2 text-xl'>
          <p className='font-bold'>{currentCountdownDate[0]}</p>
          <span>Days</span>
        </div>
        <div className='flex items-center'>-</div>
        <div className='flex flex-col items-center  p-2 text-xl'>
          <p className=' font-bold'>{currentCountdownDate[1]}</p>
          <span>Hours</span>
        </div>
        <div className='flex items-center'>:</div>
        <div className='flex flex-col items-center  p-2 text-xl'>
          <p className=' font-bold'>{currentCountdownDate[2]}</p>
          <span>Minutes</span>
        </div>
        <div className='flex items-center'>:</div>
        <div className='flex flex-col items-center  p-2 text-xl'>
          <p className=' font-bold'>{currentCountdownDate[3]}</p>
          <span>Seconds</span>
        </div>
      </div>
    )
  }
}

export default CountdownTimer
