import Link from 'next/link'
import { useRouter } from 'next/router'

const NavLink = ({ label, path }) => {
  const router = useRouter()
  const isActive = router.pathname === path

  return (
    <Link href={path}>
      <a
        className={
          `${
            isActive
              ? 'decoration-flowerGreen decoration-2 font-besley underline underline-offset-0'
              : ''
          }` + 'font-besley text-xl'
        }
      >
        {label}
      </a>
    </Link>
  )
}

const Nav = () => {
  return (
    <nav className='flex flex-row justify-around p-2 w-full'>
      <NavLink label='Announcement' path='/announcement' />
      <NavLink label='Photos' path='/photos' />
    </nav>
  )
}

export default Nav
