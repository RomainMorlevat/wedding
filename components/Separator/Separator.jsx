const Separator = () => {
  return (
    <div className='w-full border-t border-flowerGreen my-5' />
  )
}

export default Separator
