import { useState } from 'react'
import { signIn } from 'next-auth/react'

const LoginForm = () => {
  const [email, setEmail] = useState('')

  const handleOnSubmit = e => {
    e.preventDefault()
    signIn('email', { email: email, callbackUrl: `${window.location.origin}/announcement` })
  }

  return (
    <div className='flex font-sans items-center justify-center px-4'>
      <form className='space-y-3' onSubmit={handleOnSubmit}>
        <label className='sr-only' htmlFor='email'>
          Email address
        </label>

        <input
          autoComplete='username'
          className='appearance-none rounded-md relative block w-full px-3 py-2 border border-flowergray placeholder-flowergray text-flowergray focus:outline-none focus:ring-flowerGreen focus:border-flowerGreen focus:z-10 sm:text-sm'
          id='email'
          name='email'
          onChange={e => {
            setEmail(e.target.value)
          }}
          placeholder='Email address'
          required
          type='email'
        />

        <button
          className='group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-flowergray bg-flowerGreen hover:bg-flowerGreen focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-flowerGreen'
          type='submit'
        >
          Sign in with Email
        </button>
      </form>
    </div>
  )
}

export default LoginForm
