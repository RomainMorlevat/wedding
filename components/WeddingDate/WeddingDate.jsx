const WeddingDate = () => {
  const weddingDate = new Date(process.env.NEXT_PUBLIC_COUNTDOWN_DATE)

  return (
    <h2 className='flex flex-col font-bold items-center text-2xl text-slate-700'>
      {new Intl.DateTimeFormat('en-US', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      }).format(weddingDate)}
    </h2>
  )
}

export default WeddingDate
