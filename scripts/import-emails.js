require('dotenv').config({ path: '.env.local' })
const fs = require('fs')
const { Client, query } = require('faunadb')

const faunaClient = new Client({
  domain: 'db.eu.fauna.com',
  port: 443,
  scheme: 'https',
  secret: process.env.FAUNA_SECRET_KEY
})

const emails = fs
  .readFileSync('allowed-emails.txt')
  .toString('utf-8')
  .split('\n')
  .filter(el => el)

emails.forEach(email => {
  faunaClient
    .query(query.Create(query.Collection('users'), { data: { email: email } }))
    .then(ret => console.log(ret))
    .catch(err =>
      console.error('Error: [%s] %s: %s', err.name, err.message, err.errors()[0].description)
    )
})
