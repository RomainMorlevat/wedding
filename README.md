## Description

This project is a simple wedding site template where you can show useful informations to your guests.
Demo can be find here: [https://wedding-opal-iota.vercel.app/](https://wedding-opal-iota.vercel.app/)

## Technical description

This is a [Next.js](https://nextjs.org/) app.
Authorization is made with the help of [NextAuth.js](https://next-auth.js.org/) and its magic link capability. The setup is inspired by [this article](https://alterclass.io/tutorials/magic-link-authentication-in-nextjs-with-nextauth-and-fauna) from [Grégory D'Angelo](https://github.com/gdangelo) with some changes to make it work.
Project is deployed on [Vercel](https://vercel.com/).

### Fauna DB

Create an account on [Fauna](https://fauna.com/) and create a database (if not region group set to "Europe" you will have to change Fauna domain `pages/api/auth/[...nextauth].js:9`, see [documentation](https://docs.fauna.com/fauna/current/drivers/connections?lang=javascript#connection-options)).
Create an API key with server role in "Security" tab that you will paste as a value of `FAUNA_SECRET_KEY` in your `.env.local`.
In "Shell" tab, run the following:
```
CreateCollection({ name: 'accounts' });
CreateCollection({ name: 'sessions' });
CreateCollection({ name: 'users' });
CreateCollection({ name: 'verification_tokens' });
```
and
```
CreateIndex({
  name: 'account_by_provider_and_provider_account_id',
  source: Collection('accounts'),
  unique: true,
  terms: [
    { field: ['data', 'provider'] },
    { field: ['data', 'providerAccountId'] },
  ],
});
CreateIndex({
  name: 'session_by_session_token',
  source: Collection('sessions'),
  unique: true,
  terms: [{ field: ['data', 'sessionToken'] }],
});
CreateIndex({
  name: 'user_by_email',
  source: Collection('users'),
  unique: true,
  terms: [{ field: ['data', 'email'] }],
});
CreateIndex({
  name: 'verification_token_by_identifier_and_token',
  source: Collection('verification_tokens'),
  unique: true,
  terms: [{ field: ['data', 'identifier'] }, { field: ['data', 'token'] }],
});
```

#### Add know emails to Fauna DB

Users that you want to be able login must have their email set in the `users` collection. You can do that in Fauna DB admin: users collection > New Document and set a key and value "email": "email@address.com" then click on Save. 
Or you can rename allowed-emails-example.txt to allowed-emails.txt, add one email per line and run from app root `node scripts/import-emails.js` (be sure to configure it before).

## How to use

First, fork the project. Then make your changes according to your needs.

Change `.env.local.example` to `.env.local` and change keys according to your own ones.

Development server is run with:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Other informations

Wedding icon by Fabián Alexis, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

Watercolor flowers by [Harrison](https://pngset.com/download-free-png-kizqz), CC BY-NC 4.0 Licence.

Animated text, from [Akash Raj](https://akashraj9828.github.io/svg-text-animation-generator/) [with some changes](./generate_animated_text.md).
