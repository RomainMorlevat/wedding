const { fontFamily } = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    './pages/**/*.{js,jsx}',
    './components/**/*.{js,jsx}'
  ],
  theme: {
    extend: {},
    colors: {
      ...colors,
      flowerGray: '#a4a284',
      flowerGreen: '#d8dbae'
    },
    fontFamily: {
      ...fontFamily,
      besley: ['Besley'],
      cormorant: ['Cormorant']
    }
  },
  plugins: []
}
