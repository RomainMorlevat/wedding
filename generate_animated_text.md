# How to generate animated text

-   go to [Akash Raj's generator](https://akashraj9828.github.io/svg-text-animation-generator/);
-   type your text;
-   copy the svg code and paste it in `[AnimatedTitle.jsx](components/AnimatedTitle.jsx)` instead of the existing one;
-   now you must convert it to jsx syntax. Look at the <svg> tag and delete `height` and `width`, then add:
  ```
    className={`mx-auto w-10/12 md:w-8/12 ${loading ? 'opacity-0' : 'opacity-100'}`}
    preserveAspectRatio='xMinYMin meet'`
  ```
-   in the <g> tag, replace `stroke-linecap`, `fill-rule`, `font-size` and `stroke-width` with `strokeLinecap`, `fillRule`, `fontSize` and `strokeWidth`, then replace all `style=...` with `style={{ stroke: '#000', strokeWidth: '0.25mm', fill: 'none' }}` (change stroke color if you need);
-   in each <path> tags, replace `vector-effect` with `vectorEffect`;
-   you can change params in `setTextAnimation` call according to your needs.
