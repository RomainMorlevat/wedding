/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['picsum.photos', 'unsplash.com']
  },
  reactStrictMode: true
}

module.exports = nextConfig
