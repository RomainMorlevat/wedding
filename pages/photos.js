import lgZoom from 'lightgallery/plugins/zoom'
import { getSession, useSession } from 'next-auth/react'
import dynamic from 'next/dynamic'
import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react'

import Nav from '@/components/Nav/Nav'

import 'lightgallery/css/lg-zoom.css'
import 'lightgallery/css/lightgallery.css'

const LightGallery = dynamic(() => import('lightgallery/react'), {
  ssr: false
})

export default function Photos () {
  const { data: session } = useSession()

  const [photos, setPhotos] = useState(null)
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    fetch('https://picsum.photos/v2/list?page=1&limit=20')
      .then(res => res.json())
      .then(photos => {
        const reducedPhotos = photos.map(photo => {
          const isPortrait = photo.height > photo.width
          const reducedHeight = isPortrait ? 800 : 533
          const reducedWidth = isPortrait ? 533 : 800
          const reducedUrl = photo.download_url.replace(
            `${photo.width}/${photo.height}`,
            `${reducedWidth}/${reducedHeight}`
          )

          return { ...photo, download_url: reducedUrl, height: reducedHeight, width: reducedWidth }
        })
        setPhotos(reducedPhotos)
        setLoading(false)
      })
  }, [])

  if (isLoading) return <p>Loading...</p>
  if (!photos) return <p>No photos</p>

  if (session) {
    return (
      <div className='h-screen w-full'>
        <Head>
          <title>Wedding Photos</title>
          <meta name='description' content='Wedding website template' />
          <link rel='icon' href='/favicon.ico' />
        </Head>
        <Nav />
        <LightGallery elementClassNames='text-center' mode='lg-fade' plugins={[lgZoom]}>
          {photos.map(({ author, height, id, download_url: downloadUrl, url, width }) => {
            return (
              <a
                data-lg-size={`${width}-${height}`}
                className='gallery-item inline-block p-1 w-1/2 sm:w-1/4'
                data-src={downloadUrl}
                data-sub-html={`<h4>Photo by - <a href=${url}> ${author} </a> from <a href=${url}>Unsplash</a></h4>`}
                key={id}
              >
                <Image
                  alt={`Photo by ${author}`}
                  height='177'
                  src={downloadUrl}
                  width={width > height ? 266 : 117}
                />
              </a>
            )
          })}
        </LightGallery>
      </div>
    )
  }
}

export async function getServerSideProps (context) {
  const session = await getSession(context)

  if (!session) {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    }
  }

  return {
    props: { session }
  }
}
