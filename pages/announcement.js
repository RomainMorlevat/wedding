import Head from 'next/head'
import { getSession, signOut, useSession } from 'next-auth/react'

import AnimatedTitle from '@/components/AnimatedTitle/AnimatedTitle'
import CountdownTimer from '@/components/CountdownTimer/CountdownTimer'
import Events from '@/components/Events/Events'
import Footer from '@/components/Footer/Footer'
import InvitationText from '@/components/InvitationText/InvitationText'
import Nav from '@/components/Nav/Nav'
import Separator from '@/components/Separator/Separator'
import WeddingDate from '@/components/WeddingDate/WeddingDate'

export default function Announcement () {
  const { data: session } = useSession()

  if (session) {
    return (
      <div className='h-screen w-full'>
        <Head>
          <title>Wedding Announcement</title>
          <meta name='description' content='Wedding website template' />
          <link rel='icon' href='/favicon.ico' />
        </Head>

        <Nav />

        <AnimatedTitle />
        <Separator />
        <InvitationText />
        <Separator />
        <WeddingDate />
        <Separator />
        <CountdownTimer />
        <Separator />
        <Events />
        <div className='mt-5 text-center'>
          Signed in as {session.user.email} <br />
          <button
            className='inline-flex items-center px-2.5 py-1.5 border border-transparent
              text-xs font-medium rounded bg-flowerGray hover:bg-flowerGreen
              focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-flowerGray'
            onClick={() => signOut({ callbackUrl: `${window.location.origin}/` })}
          >
            Sign out
          </button>
        </div>
        <Footer />
      </div>
    )
  }
}

export async function getServerSideProps (context) {
  const session = await getSession(context)

  if (!session) {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    }
  }

  return {
    props: { session }
  }
}
