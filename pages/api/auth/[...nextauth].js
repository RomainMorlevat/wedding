import NextAuth from 'next-auth'
import EmailProvider from 'next-auth/providers/email'
import nodemailer from 'nodemailer'

import { Client as FaunaClient, query as q } from 'faunadb'
import { FaunaAdapter } from '@next-auth/fauna-adapter'

const client = new FaunaClient({
  domain: 'db.eu.fauna.com',
  port: 443,
  scheme: 'https',
  secret: process.env.FAUNA_SECRET_KEY
})

export default NextAuth({
  adapter: FaunaAdapter(client),
  callbacks: {
    async signIn ({ user, account, profile, email, credentials }) {
      const isKnowUser = await client
        .query(q.Get(q.Match(q.Index('user_by_email'), user.email)))
        .then(resp => resp)
        .catch(err =>
          console.error('Error: [%s] %s: %s', err.name, err.message, err.errors()[0].description)
        )
      if (isKnowUser) {
        return true
      } else {
        return false
      }
    }
  },
  pages: {
    error: '/auth/error',
    signIn: '/auth/signIn',
    verifyRequest: '/auth/verify-request'
  },
  providers: [
    EmailProvider({
      from: process.env.EMAIL_FROM,
      maxAge: 10 * 60, // Magic links are valid for 10 min only
      server: {
        host: process.env.EMAIL_SERVER_HOST,
        port: process.env.EMAIL_SERVER_PORT,
        auth: {
          user: process.env.EMAIL_SERVER_USER,
          pass: process.env.EMAIL_SERVER_PASSWORD
        }
      },
      async sendVerificationRequest ({ identifier: email, url, provider: { server, from } }) {
        const { host } = new URL(url)
        const transport = nodemailer.createTransport(server)
        await transport.sendMail({
          to: email,
          from,
          subject: `❤️ Sign in to ${host}`,
          text: text({ url, host }),
          html: html({ url, host, email })
        })
      }
    })
  ]
})

function html ({ url, host, email }) {
  // const escapedEmail = `${email.replace(/\./g, '&#8203;.')}`
  const escapedHost = `${host.replace(/\./g, '&#8203;.')}`
  return `
      <body style="background: #f9f9f9;">
        <table width="100%" border="0" cellspacing="20" cellpadding="0"
          style="background: #ffffff; max-width: 600px; margin: auto; border-radius: 10px;">
          <tr>
            <td align="center"
              style="padding: 10px 0px; font-size: 24px; font-family: Helvetica, Arial, sans-serif; color: #444444;">
              The wedding of Amy and Sheldon
            </td>
          </tr>
          <tr>
            <td align="center"
              style="padding: 10px 0px; font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #444444;">
              Click the button below to log in to <strong>${escapedHost}</strong>.<br />
              This button will expire in 10 minutes.
            </td>
          </tr>
          <tr>
            <td align="center" style="padding: 20px 0;">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" style="border-radius: 5px;" bgcolor="#d8dbae">
                    <a href="${url}"
                      target="_blank"
                      style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-decoration: none; border-radius 0.375rem; padding: 10px 20px; border: 1px solid #a4a284; display: inline-block; font-weight: bold;">
                      Sign in
                    </a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center"
              style="padding: 0px 0px 10px 0px; font-size: 16px; line-height: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444;">
              If you did not request this email you can safely ignore it.
            </td>
          </tr>
        </table>
      </body>
  `
}

// Fallback for non-HTML email clients
function text ({ url, host }) {
  return `Sign in to ${host}\n${url}\n\n`
}
