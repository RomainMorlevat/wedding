import { Html, Head, Main, NextScript } from 'next/document'

export default function Document () {
  return (
    <Html className='scroll-smooth bg-white antialiased'>
      <Head />
      <body className='flex flex-col h-screen justify-between font-cormorant container mx-auto sm:px-6 lg:px-8 overflow-x-auto'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
