import Head from 'next/head'

import AnimatedTitle from '@/components/AnimatedTitle/AnimatedTitle'

import Footer from '@/components/Footer/Footer'
import LoginForm from '@/components/LoginForm'

export default function SignIn () {
  return (
    <>
      <Head>
        <title>Wedding login</title>
        <meta name='description' content='Wedding website template' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <div className='flex flex-col items-center justify-center z-10'>
        <p className='mx-auto mt-20 text-center md:text-4xl text-3xl w-10/12 md:w-8/12'>
          The Wedding of
        </p>
        <AnimatedTitle />
        <LoginForm />
      </div>

      <Footer />
    </>
  )
}
