import { ExclamationCircleIcon } from '@heroicons/react/outline'

const Error = () => {
  return (
    <div className='max-w-2xl mx-auto text-center py-16 px-4 sm:py-20 sm:px-6 lg:px-8'>
      <ExclamationCircleIcon className='shrink-0 w-12 h-12 text-flowerGreen m-auto' />
      <h2 className='text-3xl font-extrabold sm:text-4xl'>
        <span className='block font-besley font-thin mb-4'>Your email address is not recognized.</span>
        <span className='block font-medium'>Please send it to the future groom so he can authorize it.</span>
      </h2>
    </div>
  )
}

export default Error
